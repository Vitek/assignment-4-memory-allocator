#include "tests.h"


#define TEST_SIZE 1024
#define HEAP_SIZE 16384
#define COUNTS 8

#ifndef MAP_FIXED_NOREPLACE
#define MAP_FIXED_NOREPLACE 0x100000
#endif

static struct block_header* block_get_header(void* contents) {
    return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}

static bool test1() {
    printf("Test 1: successful malloc \n");
    void *heap = heap_init(HEAP_SIZE);
    void *block = _malloc(TEST_SIZE);

    if (heap == NULL) {
        printf("Test 1: heap init error \n");
        return false;
    }
    if (block == NULL) {
        printf("Test 1: malloc error \n");
        return false;
    }

    debug_heap(stdout, heap);
    _free(block);
    munmap(heap, size_from_capacity((block_capacity){.bytes = HEAP_SIZE}).bytes);
    printf("Test 1 passed \n");
    return true;
}

static bool test2() {
    printf("Test 2: free one block \n");

    void *heap = heap_init(HEAP_SIZE);
    void *blocks[COUNTS];
    for (size_t i=0; i<COUNTS; i++) {
        blocks[i]=_malloc(TEST_SIZE);
    }
    struct block_header* headers[COUNTS];
    for (size_t i=0; i<COUNTS; i++) {
        headers[i] = block_get_header(blocks[i]);
    }

    for (size_t i=0; i<COUNTS; i++) {
        if (blocks[i] == NULL || headers[i]->capacity.bytes < TEST_SIZE) {
            printf ("Test 2: creating block %zu error \n", i);
            return false;
        }
    }

    debug_heap(stdout, heap);

    _free(blocks[4]);
    if (headers[4]->is_free == 0) {
        printf( "Test 2: free %zu block error\n", (size_t) 4);
        debug_heap(stdout, heap);
        return false;
    }

    debug_heap(stdout, heap);

    for (size_t i=0; i<COUNTS; i++) {
        _free(blocks[COUNTS-i-1]);
    }

    debug_heap(stdout, heap);
    munmap(heap, size_from_capacity(headers[0]->capacity).bytes);
    printf("Test 2 passed \n");
    return true;
}

static bool test3() {
    printf("Test 3: free two blocks \n");

    void *heap = heap_init(HEAP_SIZE);
    void *blocks[COUNTS];
    for (size_t i=0; i<COUNTS; i++) {
        blocks[i]=_malloc(TEST_SIZE);
    }
    struct block_header* headers[COUNTS];
    for (size_t i=0; i<COUNTS; i++) {
        headers[i] = block_get_header(blocks[i]);
    }

    for (size_t i=0; i<COUNTS; i++) {
        if (!blocks[i] || headers[i]->capacity.bytes < TEST_SIZE) {
            printf ("Test 3: creating block %zu error \n", i);
            return false;
        }
    }

    debug_heap(stdout, heap);

    _free(blocks[4]);
    if (headers[4]->is_free == 0) {
        printf( "Test 3: free %zu block error\n", (size_t) 4);
        debug_heap(stdout, heap);
        return false;
    }

    _free(blocks[5]);
    if (headers[5]->is_free == 0) {
        printf( "Test 3: free %zu block error\n", (size_t) 5);
        debug_heap(stdout, heap);
        return false;
    }

    debug_heap(stdout, heap);

    for (size_t i=0; i<COUNTS; i++) {
        _free(blocks[COUNTS-i-1]);
    }

    debug_heap(stdout, heap);
    munmap(heap, size_from_capacity(headers[0]->capacity).bytes);
    printf("Test 3 passed \n");
    return true;
}

static bool test4() {
    printf("Test 4: expand region \n");

    void *heap = heap_init(HEAP_SIZE);
    void *blocks[COUNTS];
    for (size_t i=0; i<COUNTS; i++) {
        blocks[i]=_malloc(i*8*TEST_SIZE);
    }
    struct block_header* headers[COUNTS];
    for (size_t i=0; i<COUNTS; i++) {
        headers[i] = block_get_header(blocks[i]);
    }

    for (size_t i=0; i<COUNTS; i++) {
        if (!blocks[i] || headers[i]->capacity.bytes < i*8*TEST_SIZE) {
            printf ("Test 4: creating block %zu error \n", i);
            return false;
        }
    }

    debug_heap(stdout, heap);

    for (size_t i=0; i<COUNTS; i++) {
        _free(blocks[COUNTS-i-1]);
    }

    debug_heap(stdout, heap);
    munmap(heap, size_from_capacity(headers[0]->capacity).bytes);
    printf("Test 4 passed \n");
    return true;
}

static bool test5() {
    printf("Test 5: non expand region after space \n");
    void *heap = heap_init(capacity_from_size((block_size){(size_t) HEAP_SIZE}).bytes);
    void *block1 = _malloc(capacity_from_size((block_size){(size_t) HEAP_SIZE}).bytes);
    struct block_header *block_header1 = block_get_header(block1);

    if (block1 == NULL || block_header1->capacity.bytes < capacity_from_size((block_size){(size_t) HEAP_SIZE}).bytes) {
        printf ("Test 5: creating block %zu error \n", (size_t) 1);
        return false;
    }

    debug_heap(stdout, heap);
    void *space = mmap(block_header1->contents + block_header1->capacity.bytes,
                       HEAP_SIZE, PROT_READ | PROT_WRITE,
                       MAP_PRIVATE | MAP_ANONYMOUS | MAP_FIXED_NOREPLACE, -1, 0);

    if (space == MAP_FAILED) {
        printf ("Test 5: creating space between blocks error \n");
        return false;
    }

    void *block2 = _malloc(HEAP_SIZE);
    struct block_header *block_header2 = block_get_header(block2);
    debug_heap(stdout, heap);


    if (block_header1->next != block_header2 || block_header1->contents + block_header1->capacity.bytes == (void *)block_header2) {
        printf ("Test 5: creating space between blocks error \n");
        return false;
    }

    _free(block2);
    _free(block1);
    debug_heap(stdout, heap);
    munmap(heap, size_from_capacity(block_header1 ->capacity).bytes);
    munmap(space, HEAP_SIZE);
    printf ("Test 5: passed \n");
    return true;
}

test_function tests[] = {
    test1,  test2,  test3,  test4,  test5
};

void run_tests() {
    printf("\n");
    printf("Testing started\n");
    bool err=false;
    for (size_t i = 0; i < sizeof(tests) / sizeof(tests[0]); i++) {
        if (!tests[i]) {
            err=true;
        }
    }
    if (!err) {
        printf("Tests past\n");
    } else {
        printf("Tests failed\n");
    }
}
